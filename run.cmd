echo off
set Drive=%~d0
set PATH=%~dp0;%PATH%

echo on
start /min java -jar %~dp0/selenium-server-standalone-2.44.0.jar

if exists %~dp0/../xampp/xampp_start goto DottedStart

if exists C:/xampp/xampp_start goto CStart
start /min /b I:/leerlingen/xampp/xampp_start &&  start I:/leerlingen/xampp/xampp-control.exe
goto END

::CStart
start /min /b C:/xampp/xampp_start &&  start C:/xampp/xampp-control.exe
goto END

::DottedStart
start /min /b %~dp0/../xampp/xampp_start &&  start %~dp0/../xampp/xampp-control.exe

::END