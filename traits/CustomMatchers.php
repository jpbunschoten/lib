<?php

trait CustomMatchers  {

    public function assertIsJSONObject($JSONString) {
        json_decode($JSONString);
        return (json_last_error() == JSON_ERROR_NONE);
    }

}
