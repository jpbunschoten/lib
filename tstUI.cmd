@echo off
rem echo %cd%
rem echo %~dp0
set CWD=%~dp0
set AL=%~dp0src\autoload.php

cls

REM :: phpunit --bootstrap %AL% %CWD%tests\%*

::echo on    
REM phpunit  %CWD%UItests\%1 %2 %3 %4 %5 %6 %7 %8 %9
php.exe C:/xampp/htdocs/lib/phpunit.phar --bootstrap %CWD%UItests\bootstrap.php %CWD%UItests\%1 %2 %3 %4 %5 %6 %7 %8 %9
@echo off

