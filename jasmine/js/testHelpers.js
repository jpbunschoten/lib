
/**
 * Tel elementen in een object
 * @param {type} obj
 * @returns {Number}
 */
countObj = function (obj) {
    var cnt = 0;
    for (var prop in obj) {
        if (obj.hasOwnProperty(prop))
            cnt++;
    }
    return cnt;
};
