<?php

function A($a = 7) {
	return $a;
}
function B($b) {
	return $b + A();
}
function C($b) {
	return B($b);
}
