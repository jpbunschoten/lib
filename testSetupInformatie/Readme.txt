In deze map vind je informatie over het opzetten van PHP unit testen

Op je PC (thuis/school) heb je een webserver geinstalleerd (apachefriends.org). Deze staat in C:/xampp
Op je PC (thuis/school) staat in C:/bin de bestanden van de folder hierboven (dus via ..)
Op je PC (thuis/school) ga je naar C:/bin en voer je de volgende commando's uit
    
    composer.cmd
    
Mocht je xampp nieuw geinstalleerd hebben kun je altijd 'setup_xammp.cmd' uitvoeren

In je project (laten we die maar ProjectX noemen, heb je je PHP code die je wilt laten testen. 
Je maakt in je project map '/tests'  aan. Je hebt dan ProjectX/tests


In ProjectX plaats je 
    composer.json
    run.cmd
    phpunit.xml
    example.php
    
In ProjectX/tests plaats je
    bootstrap.php
    exampleTest.php
    
    
Het testen:
open 
    run.cmd 
Je krijgt nu een command prompt window.

Type in de command window nu

    cls && phpunit

Je test(en) wordt/worden nu uitgevoerd.
