<?php

include_once "example.php";


/**
 * test the example
 */
class ExampleTest extends PHPUnit_Framework_TestCase {	// class must end on Test

   /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function setUp() {
        
    }

    /**
     * Tears down the fixture, for example, closes a network connection.
     * This method is called after a test is executed.
     */
    protected function tearDown() {
        
    }
	public function testAlive() {
		$this->assertTrue(true);	// must give the desired '.'
	}

    public function testA() {
        $this->assertEquals(7, A(), "default");
		$this->assertEquals(10, A(10), "set to 10");        
    }

    public function testB() {
        $this->assertEquals(7, B(0), "default");
		$this->assertEquals(10, B(3), "set to 10");
    }
	public function testC() {
		// fails to show that
        $this->assertEquals("C", C(0), "default");
		
    }
}
