<?php

include_once __DIR__ . '/bootstrap.php';

class ClientComparisonsTest extends PHPUnit_Framework_TestCase {

    use WebDriverAssertions;

use WebDriverDevelop;

use WebDriverIF;

use WebDriverMethodsIF;

    protected $urlBase = "http://booking.uz.gov.ua/";
    private $wd;

    public function testOne() {
        $this->wd = $this->getWebDriver();

        $from = $this->fillStation(WebDriverBy::name('station_from'), $this->fromInput);
        $this->pickStattion(WebDriverBy::id('stations_from'), $this->from);
        $this->assertEquals("Kyiv", $from->getAttribute('value'));

        $to = $this->fillStation(WebDriverBy::name('station_till'), $this->toInput);
        $this->pickStattion(WebDriverBy::id('stations_till'), $this->to);
        $this->assertEquals("Lviv", $to->getAttribute('value'));

        $this->forTomorrow();

        $this->wd->findElement(WebDriverBy::name('search'))->click();

        $result = $this->trains();

        (new WebDriverWait($this->wd, 20))
                ->until(WebDriverExpectedCondition::visibilityOf($result));
    }

}
