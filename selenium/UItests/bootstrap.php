<?php

require_once __DIR__ . '/../vendor/autoload.php'; // added JBN

require_once __DIR__ . '/support/WebDriverAssertions.php';
require_once __DIR__ . '/support/WebDriverDevelop.php';
require_once __DIR__ . '/support/WebDriverIF.php';
require_once __DIR__ . '/support/WebDriverMethodsIF.php';
