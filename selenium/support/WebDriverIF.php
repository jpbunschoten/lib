<?php

use Facebook\WebDriver\Remote\DesiredCapabilities as DesiredCapabilities;
use Facebook\WebDriver\Remote\RemoteWebDriver;

trait webDriverIF {

    private static $host = 'http://localhost:4444/wd/hub'; // this is the default
    private static $mSECONDS = 50; // in msec
    protected static $driver;       // remoteWebDriver
    private static $capabilities;

    protected static function setWebDriver($d) {
        self::$driver = $d;
    }

    protected static function getWebDriver() {
        return self::$driver;
    }

    protected function getBrowser() {
        $browser = self::$browser;
        self::$capabilities = DesiredCapabilities::$browser();
    }

    protected function getRemoteWebDriver() {
        if (self::$browser == 'firefox') {
            return RemoteWebDriver::create(self::$host, self::$capabilities, self::$mSECONDS);
        } else if (self::$browser == 'chrome') {
            return Facebook\WebDriver\Chrome\ChromeDriver::start();
        } else {
            throw new Exception("browser " . self::$browser . " not recognized");
        }
    }

    protected function doSetUp() {
        self::getBrowser();
        self::setWebDriver(self::getRemoteWebDriver());
    }

    protected function doTearDown() {
        try {
            $wd = self::getWebDriver();
            $wd->close();
            $wd->quit();
        } catch (NoSuchWindowException $e) {
            
        }
        //catch (Exception e) {}
    }

    public static function setUpBeforeClass() {
        self::doSetUp();
    }

    public static function tearDownAfterClass() {
        self::doTearDown();
    }

    public function tearDown() {
        $wd = self::getWebDriver();
        $handles = $wd->getWindowHandles();
        foreach ($handles as $i => $handle) {
            if ($i == 0) continue;
            //$wd->switchTo()->window($handle)->close();
        }
    }

}
